# Pega-test1
## Versions
- Php 7.4.4
- Symfony 5.0.7
- Bootstrap 4.4.1

## Installation

```
composer install
```

For accessing Twitter API:
Add your TWITTER API keys to your .env file:
```
TWITTER_API_KEY=
TWITTER_API_SECRET=
TWITTER_ACCESS_TOKEN=
TWITTER_ACCESS_TOKEN_SECRET=
```
And finally start the built in web server to test te page at *http://localhost:8000*
```
symfony server:start
```
Or you can use the a composer script:
```
composer serve
```
## Docker:
From the project directory:

Add Twitter API keys to .env file see above.

Build the container:
```
docker-compose build php
```
Run the container
```
docker-compose up
```
Open the terminal from the composer
```
docker exec -it pega-test1 bash
```
Run the php built in web server with a composer script
```
composer serve
``` 
## Tests:
There is a composer script to run all tests:
```
composer test
```
#### BDD Tests:
Tested on MAC and this functions are not available from the "php" container yet.
```
brew cask install chromedriver
```

Start Selenium driver:
Docker
```
docker run -p 4444:4444 selenium/standalone-chrome:2.53.1
```
Java:
```
curl -L http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar > selenium-server-standalone-2.53.1.jar
java -jar selenium-server-standalone-2.53.1.jar
```

### Feature improvements
 - Build more Unit tests to improve test coverages
 - Build more BDD test
 - Add some refactor to improve communication with Twitter API
 - ...