Feature:
  Basic bdd tests

  Scenario Outline: When I fill the task form with valid data I can submit it and get a result.
    Given I am on "/"
    When  I fill in "task_handle1" with "<handle1>"
    And   I fill in "task_handle2" with "<handle2>"
    And   I fill in "task_method" with "<method>"
    And   I press "Submit"
    Then  I should be on "/<handle1>/<handle2>/<method>"
    And   I should see "Pega Test 1 - Result"
    And   I should see xpath "//a[text()='Back']"
    And   I should see xpath "//div[contains(@class, 'result')]"

    Examples:
      | handle1 | handle2 | method |
      | symfony | knplabs | fib    |
      | symfony | knplabs | mod    |
      | potus   | github  | fib    |
      | potus   | github  | mod    |

  Scenario Outline: I should get an error message if handle1 equals to handle2
    Given I am on "/"
    When  I fill in "task_handle1" with "<handle1>"
    And   I fill in "task_handle2" with "<handle1>"
    And   I fill in "task_method" with "<method>"
    And   I press "Submit"
    Then  I should see "Handle1 can not be equal with handle2"

    Examples:
      | handle1 | method |
      | symfony | fib    |
      | symfony | mod    |
      | potus   | fib    |
      | potus   | mod    |
      | github  | fib    |
      | github  | mod    |

  Scenario Outline: When I fill the task form with invalid data I can submit it and get a message "No results has found..." instead of result.
    Given I am on "/"
    When  I fill in "task_handle1" with "<handle1>"
    And   I fill in "task_handle2" with "<handle2>"
    And   I fill in "task_method" with "<method>"
    And   I press "Submit"
    Then  I should be on "/<handle1>/<handle2>/<method>"
    And   I should see "Pega Test 1 - Result"
    And   I should see "No results has found..."
    And   I should see xpath "//a[text()='Back']"

    Examples:
      | handle1 | handle2          | method |
      | laravel | gergregrmgwfwemf | fib    |
      | laravel | gergregrmgwfwemf | mod    |


