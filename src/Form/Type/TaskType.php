<?php

namespace App\Form\Type;

use App\Task\Enum\MethodEnum;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class TaskType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('handle1', TextType::class, [
                'label' => 'Handle1:',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Handle1 can not be blank'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-z\-0-9]+$/i',
                        'htmlPattern' => '^[a-zA-Z\-0-9]+$',
                        'message' => 'Handle1 must be alphanumeric',
                    ])
                ]
            ])
            ->add('handle2', TextType::class, [
                'label' => 'Handle2:',
                'error_bubbling' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Handle2 can not be blank'
                    ]),
                    new Regex([
                        'pattern' => '/^[a-z\-0-9]+$/i',
                        'htmlPattern' => '^[a-zA-Z\-0-9]+$',
                        'message' => 'Handle2 must be alphanumeric',
                    ])
                ]
            ])
            ->add('method', ChoiceType::class, [
                'choices' => MethodEnum::getChoices(),
                'label' => 'Method:',
                'error_bubbling' => true
            ])
            ->add('submit', SubmitType::class);
    }
}