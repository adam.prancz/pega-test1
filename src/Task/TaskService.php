<?php

namespace App\Task;

use App\Task\Client\ClientInterface;
use App\Task\Entity\Post;
use App\Task\IndexGenerator\IndexGeneratorInterface;

class TaskService
{
    /** @var ClientInterface|null */
    private $sourceClient1 = null;

    /** @var ClientInterface|null */
    private $sourceClient2 = null;

    /** @var ClientInterface|null */
    private $sourceClient3 = null;

    /** @var IndexGeneratorInterface|null */
    private $indexGenerator = null;

    /**
     * @return Post[]
     */
    public function doReplace(): array
    {
        $sourceTransformer1 = $this->getSourceClient1()->getResponseTransformer();
        $sourceTransformer2 = $this->getSourceClient2()->getResponseTransformer();
        $sourceTransformer3 = $this->getSourceClient3()->getResponseTransformer();
        $data1 = $sourceTransformer1->transform($this->getSourceClient1()->getAll());
        $data2 = $sourceTransformer2->transform($this->getSourceClient2()->getAll());
        $data3 = $sourceTransformer3->transform($this->getSourceClient3()->getAll());

        $replaceCounter = 0;
        $indexes = $this->indexGenerator->getIndexes();

        $data = $this->combArray($data1, $data2);

        foreach ($data as $key => $value) {
            if (in_array($key, $indexes)) {
                if (isset($data3[$replaceCounter])) {
                    $data[$key] = $data3[$replaceCounter];
                } else {
                    $data[$key] = new Post();
                }

                $replaceCounter++;
            }
        }

        return $data;
    }

    /**
     * @return ClientInterface|null
     */
    public function getSourceClient1(): ?ClientInterface
    {
        return $this->sourceClient1;
    }

    /**
     * @param ClientInterface|null $sourceClient1
     *
     * @return TaskService
     */
    public function setSourceClient1(?ClientInterface $sourceClient1): ?TaskService
    {
        $this->sourceClient1 = $sourceClient1;

        return $this;
    }

    /**
     * @return ClientInterface|null
     */
    public function getSourceClient2(): ?ClientInterface
    {
        return $this->sourceClient2;
    }

    /**
     * @param ClientInterface|null $sourceClient2
     *
     * @return TaskService
     */
    public function setSourceClient2(?ClientInterface $sourceClient2): ?TaskService
    {
        $this->sourceClient2 = $sourceClient2;

        return $this;
    }

    /**
     * @return ClientInterface|null
     */
    public function getSourceClient3(): ?ClientInterface
    {
        return $this->sourceClient3;
    }

    /**
     * @param ClientInterface|null $sourceClient3
     *
     * @return TaskService
     */
    public function setSourceClient3(?ClientInterface $sourceClient3): ?TaskService
    {
        $this->sourceClient3 = $sourceClient3;

        return $this;
    }

    /**
     * @param array $data1
     * @param array $data2
     *
     * @return array
     */
    public function combArray(array $data1 = [], array $data2 = []): array
    {
        $combedArray = array_merge($data1, $data2);

        if (!empty($combedArray)) {
            usort($combedArray, function (Post $item1, Post $item2) {
                return $item1->getTimeStamp() < $item2->getTimeStamp();
            });

            return array_combine(range(1, count($combedArray)), array_values($combedArray));
        }

        return [];
    }

    /**
     * @return IndexGeneratorInterface|null
     */
    public function getIndexGenerator(): ?IndexGeneratorInterface
    {
        return $this->indexGenerator;
    }

    /**
     * @param IndexGeneratorInterface|null $indexGenerator
     *
     * @return TaskService
     */
    public function setIndexGenerator(?IndexGeneratorInterface $indexGenerator): ?TaskService
    {
        $this->indexGenerator = $indexGenerator;

        return $this;
    }
}