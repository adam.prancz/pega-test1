<?php

namespace App\Task\Entity;

use DateTime;
use Exception;

class Post
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /** @var string */
    protected $id;

    /** @var string */
    protected $source = '';

    /** @var string */
    protected $message = '';

    /** @var int */
    protected $timeStamp = 0;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Post
     */
    public function setId(string $id): Post
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return Post
     */
    public function setMessage(string $message): Post
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return Post
     */
    public function setSource(string $source): Post
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @param DateTime|null $date
     *
     * @return Post
     */
    public function setDate(DateTime $date = null): Post
    {
        $this->timeStamp = $date->getTimestamp();

        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getDisplayDate(): string
    {
        $date = '-';

        if (!empty($this->getTimeStamp())) {
            $dateTime = new DateTime();
            $dateTime->setTimestamp($this->getTimeStamp());
            $date = $dateTime->format(self::DATE_FORMAT);
        }

        return $date;
    }

    /**
     * @return int
     */
    public function getTimeStamp(): int
    {
        return $this->timeStamp;
    }

    /**
     * @param int $timeStamp
     *
     * @return Post
     */
    public function setTimeStamp(int $timeStamp): Post
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }
}
