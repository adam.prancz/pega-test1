<?php

namespace App\Task\Client;

/**
 * Api documentation: https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
 */
class Twitter extends BaseClient
{
    /** @var string */
    protected $handle = '';

    /** @var string */
    protected $apiUrl = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

    /**
     * Curl version from: https://stackoverflow.com/questions/12916539/simplest-php-example-for-retrieving-user-timeline-with-twitter-api-version-1-1
     * For null handle it gets back the owner time line
     *
     * @return string
     */
    public function getAll(): string
    {
        // TODO: Refactor and use Guzzle
        $token = getenv('TWITTER_ACCESS_TOKEN');
        $token_secret = getenv('TWITTER_ACCESS_TOKEN_SECRET');
        $consumer_key = getenv('TWITTER_API_KEY');
        $consumer_secret = getenv('TWITTER_API_SECRET');

        $host = 'api.twitter.com';
        $method = 'GET';
        $path = '/1.1/statuses/user_timeline.json'; // api call path

        $query = [ // query parameters
            'screen_name' => $this->getHandle(),
            'count' => (string)$this->getNumberOfPostsToGet(),
        ];

        $oauth = [
            'oauth_consumer_key' => $consumer_key,
            'oauth_token' => $token,
            'oauth_nonce' => (string)mt_rand(), // a stronger nonce is recommended
            'oauth_timestamp' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_version' => '1.0'
        ];

        $oauth = array_map("rawurlencode", $oauth); // must be encoded before sorting
        $query = array_map("rawurlencode", $query);

        $arr = array_merge($oauth, $query); // combine the values THEN sort

        asort($arr); // secondary sort (value)
        ksort($arr); // primary sort (key)

        // http_build_query automatically encodes, but our parameters
        // are already encoded, and must be by this point, so we undo
        // the encoding step
        $querystring = urldecode(http_build_query($arr, '', '&'));

        $url = "https://$host$path";

        // mash everything together for the text to hash
        $base_string = $method . "&" . rawurlencode($url) . "&" . rawurlencode($querystring);

        // same with the key
        $key = rawurlencode($consumer_secret) . "&" . rawurlencode($token_secret);

        // generate the hash
        $signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));

        // this time we're using a normal GET query, and we're only encoding the query params
        // (without the oauth params)
        $url .= "?" . http_build_query($query);
        $url = str_replace("&amp;", "&", $url); //Patch by @Frewuill

        $oauth['oauth_signature'] = $signature; // don't want to abandon all that work!
        ksort($oauth); // probably not necessary, but twitter's demo does it

        // also not necessary, but twitter's demo does this to
        $oauth = array_map(function ($str) {
            return '"' . $str . '"';
        }, $oauth);

        // this is the full value of the Authorization line
        $auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));

        // if you're doing post, you need to skip the GET building above
        // and instead supply query parameters to CURLOPT_POSTFIELDS
        $options = [
            CURLOPT_HTTPHEADER => ["Authorization: $auth"],
            //CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);

        return $json;
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param string $handle
     *
     * @return ClientInterface
     */
    public function setHandle(string $handle = ''): ClientInterface
    {
        $this->handle = $handle;

        return $this;
    }
}