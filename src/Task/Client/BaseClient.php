<?php

namespace App\Task\Client;

use App\Task\Transformer\TransformerInterface;

abstract class BaseClient implements ClientInterface
{
    /** @var TransformerInterface|null */
    protected $responseTransformer = null;

    /** @var string */
    protected $apiUrl = '';

    /** @var int */
    protected $numberOfPostsToGet = 1;

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->apiUrl;
    }

    /**
     * @return int
     */
    public function getNumberOfPostsToGet(): int
    {
        return $this->numberOfPostsToGet;
    }

    /**
     * @param int $numberOfPostsToGet
     *
     * @return ClientInterface
     */
    public function setNumberOfPostsToGet(int $numberOfPostsToGet = 1): ClientInterface
    {
        $this->numberOfPostsToGet = $numberOfPostsToGet;

        return $this;
    }

    /**
     * @return TransformerInterface|null
     */
    public function getResponseTransformer(): TransformerInterface
    {
        return $this->responseTransformer;
    }

    /**
     * @param TransformerInterface|null $responseTransformer
     *
     * @return self
     */
    public function setResponseTransformer(TransformerInterface $responseTransformer): ClientInterface
    {
        $this->responseTransformer = $responseTransformer;

        return $this;
    }
}