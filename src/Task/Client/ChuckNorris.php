<?php

namespace App\Task\Client;

use GuzzleHttp\Client;

/**
 * Api documentation: http://www.icndb.com/api/
 */
class ChuckNorris extends BaseClient
{
    /** @var string */
    protected $handle = '';

    /** @var string */
    protected $apiUrl = 'http://api.icndb.com/jokes/random';

    /**
     * @return string
     */
    public function getAll(): string
    {
        $client = new Client();

        $response = $client->get($this->getApiUrl() . '/' . $this->getNumberOfPostsToGet());

        if ($response->getBody()) {
            return $response->getBody()->getContents();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param string $handle
     *
     * @return ClientInterface
     */
    public function setHandle(string $handle): ClientInterface
    {
        $this->handle = $handle;

        return $this;
    }
}