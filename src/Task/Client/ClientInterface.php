<?php

namespace App\Task\Client;

use App\Task\Transformer\TransformerInterface;

interface ClientInterface
{
    /**
     * @return string
     */
    public function getAll(): string;

    /**
     * @param int $numberOfPostsToGet
     *
     * @return ClientInterface
     */
    public function setNumberOfPostsToGet(int $numberOfPostsToGet = 1): ClientInterface;

    /**
     * @return TransformerInterface
     */
    public function getResponseTransformer(): TransformerInterface;

    /**
     * @param TransformerInterface $responseTransformer
     *
     * @return ClientInterface
     */
    public function setResponseTransformer(TransformerInterface $responseTransformer): ClientInterface;
}