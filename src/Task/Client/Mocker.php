<?php

namespace App\Task\Client;

class Mocker extends BaseClient
{
    const TEST_MESSAGE = 'This is a mock message from the %s handle';
    const START_DATE = '10 September 2019';
    const END_DATE = '10 Marc 2020';
    const DATE_FORMAT = 'Y-m-d H:i:s';
    const SOURCE_NAME = 'mocker/%s';

    /** @var string */
    protected $handle = '';

    /**
     * @return string
     */
    public function getAll(): string
    {
        $posts = [];

        for ($i = 1; $i <= $this->getNumberOfPostsToGet(); $i++) {
            $posts[] = $this->generatePost();
        }

        return json_encode($posts);
    }

    /**
     * @return array
     */
    public function generatePost(): array
    {
        return [
            'source' => $this->getSourceName(),
            'date' => $this->generateRandomDate(),
            'message' => sprintf(self::TEST_MESSAGE, $this->getHandle())
        ];
    }

    /**
     * @return string
     */
    private function getSourceName(): string
    {
        return sprintf(self::SOURCE_NAME, $this->getHandle());
    }

    /**
     * @return string
     */
    public function getHandle(): string
    {
        return $this->handle;
    }

    /**
     * @param string $handle
     *
     * @return ClientInterface
     */
    public function setHandle(string $handle): ClientInterface
    {
        $this->handle = $handle;

        return $this;
    }

    /**
     * @return string
     */
    public function generateRandomDate(): string
    {
        $start = strtotime(self::START_DATE);
        $end = strtotime(self::END_DATE);
        $timestamp = mt_rand($start, $end);

        return date(self::DATE_FORMAT, $timestamp);
    }
}