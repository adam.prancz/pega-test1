<?php

namespace App\Task\Transformer;

use App\Task\Entity\Post;
use Exception;

class ChuckNorrisToPost implements TransformerInterface
{
    /**
     * @param string $response
     *
     * @return Post[]
     * @throws Exception
     */
    public function transform(string $response = ''): array
    {
        $posts = [];
        $response = json_decode($response, true);
        $index = 1;

        if (!empty($response) && $response['type'] === 'success') {
            foreach ($response['value'] as $item) {
                $post = new Post();
                $post
                    ->setId($index)
                    ->setSource('icndb')
                    ->setMessage($item['joke'])
                ;
                $posts[] = $post;
                $index++;
            }
        }

        return $posts;
    }
}