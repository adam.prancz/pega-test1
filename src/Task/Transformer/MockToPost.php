<?php

namespace App\Task\Transformer;

use App\Task\Entity\Post;
use DateTime;

class MockToPost implements TransformerInterface
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @param string $response
     *
     * @return Post[]
     */
    public function transform(string $response = ''): array
    {
        $posts = [];
        $response = json_decode($response, true);
        $index = 1;

        if (!empty($response) && is_array($response)) {
            foreach ($response as $item) {
                $date = DateTime::createFromFormat(self::DATE_FORMAT, $item['date']);
                $post = new Post();
                $post
                    ->setId($index)
                    ->setMessage($item['message'])
                    ->setSource($item['source'])
                    ->setDate($date);
                $posts[] = $post;
                $index++;
            }
        }

        return $posts;
    }
}