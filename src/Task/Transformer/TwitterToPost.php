<?php

namespace App\Task\Transformer;

use App\Task\Entity\Post;
use DateTime;
use Exception;

class TwitterToPost implements TransformerInterface
{
    /**
     * @param string $response
     *
     * @return Post[]
     * @throws Exception
     */
    public function transform(string $response = ''): array
    {
        $posts = [];
        $response = json_decode($response, true);
        $index = 1;

        if (isset($response['errors']) || isset($response['error'])) {
            return $posts;
        }

        if (!empty($response) && is_array($response)) {
            if (!isset($response['error']) || !isset($response['errors'])) {
                foreach ($response as $item) {
                    $date = new DateTime($item['created_at']);
                    $post = new Post();
                    $post
                        ->setId($index)
                        ->setMessage($item['text'])
                        ->setSource('twitter/' . $item['user']['screen_name'])
                        ->setDate($date);
                    $posts[] = $post;
                    $index++;
                }
            }
        }

        return $posts;
    }
}