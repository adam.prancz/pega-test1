<?php

namespace App\Task\Transformer;

use App\Task\Entity\Post;

interface TransformerInterface
{
    /**
     * @param string $response
     *
     * @return Post[]
     */
    function transform(string $response = ''): array;
}