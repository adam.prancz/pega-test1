<?php

namespace App\Task\IndexGenerator;

abstract class BaseGenerator implements IndexGeneratorInterface
{
    /** @var int */
    private $numberOfIndex = 1;

    /**
     * @return int
     */
    public function getNumberOfIndex(): int
    {
        return $this->numberOfIndex;
    }

    /**
     * @param int $numberOfIndex
     *
     * @return IndexGeneratorInterface
     */
    public function setNumberOfIndex(int $numberOfIndex = 1): IndexGeneratorInterface
    {
        $this->numberOfIndex = $numberOfIndex;

        return $this;
    }
}
