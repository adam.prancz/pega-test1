<?php

namespace App\Task\IndexGenerator;

class FibonacciGenerator extends BaseGenerator
{
    const START_INDEX = 2;

    /**
     * @return int[]
     */
    public function getIndexes(): array
    {
        $indexes = [];

        if ($this->getNumberOfIndex() > self::START_INDEX) {
            $f1 = 0;
            $f2 = 1;

            do {
                $f3 = $f1 + $f2;
                $f1 = $f2;
                $f2 = $f3;
                if ($f3 > self::START_INDEX) {
                    $indexes[] = $f3;
                }
            } while (($f1+$f2) < $this->getNumberOfIndex());
        }

        return $indexes;
    }
}
