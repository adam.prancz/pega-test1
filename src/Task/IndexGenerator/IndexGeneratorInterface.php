<?php

namespace App\Task\IndexGenerator;

interface IndexGeneratorInterface
{
    /**
     * @return int[]
     */
    public function getIndexes(): array;

    /**
     * @param int $numberOfPostsToGet
     *
     * @return IndexGeneratorInterface
     */
    public function setNumberOfIndex(int $numberOfPostsToGet = 1): IndexGeneratorInterface;
}