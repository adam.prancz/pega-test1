<?php

namespace App\Task\IndexGenerator;

class ModGenerator extends BaseGenerator
{
    /**
     * @return int[]
     */
    public function getIndexes(): array
    {
        $indexes = [];
        $index = 3;

        if ($this->getNumberOfIndex() >= 3) {
            do {
                $indexes[] = $index;
                $index = $index + 3;
            } while ($index <= $this->getNumberOfIndex());
        }

        return $indexes;
    }
}
