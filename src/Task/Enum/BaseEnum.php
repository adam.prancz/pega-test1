<?php

namespace App\Task\Enum;

use ReflectionClass;
use ReflectionException;

abstract class BaseEnum
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public static function getChoices(): array
    {
        $reflection = new ReflectionClass(get_called_class());
        $constants = $reflection->getConstants();

        $values = array_map(function ($value)  {
            return $value;
        }, $constants);

        return array_combine($constants, $values);
    }

    /**
     * @param string $choice
     *
     * @return string
     * @throws ReflectionException
     */
    public static function getChoice(string $choice): string
    {
        $choices = self::getChoices();
        return $choices[$choice];
    }
}