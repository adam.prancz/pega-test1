<?php

namespace App\Task\Enum;

class MethodEnum extends BaseEnum
{
    const MOD = 'mod';
    const FIBONACCI = 'fib';
}