<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Task
{
    /** @var string */
    protected $handle1 = '';

    /** @var string */
    protected $handle2 = '';

    /** @var string */
    protected $method = 'fib';

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata->addConstraint(new Assert\Callback('validate'));
    }

    /**
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $context, $payload): void
    {
        if ($this->getHandle1() === $this->getHandle2()) {
            $context->buildViolation('Handle1 can not be equal with handle2')
                ->atPath('handle2')
                ->addViolation();
        }
    }

    /**
     * @return string
     */
    public function getHandle1(): string
    {
        return $this->handle1;
    }

    /**
     * @param string $handle1
     *
     * @return Task
     */
    public function setHandle1(string $handle1): Task
    {
        $this->handle1 = $handle1;

        return $this;
    }

    /**
     * @return string
     */
    public function getHandle2(): string
    {
        return $this->handle2;
    }

    /**
     * @param string $handle2
     *
     * @return Task
     */
    public function setHandle2(string $handle2): Task
    {
        $this->handle2 = $handle2;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return Task
     */
    public function setMethod(string $method): Task
    {
        $this->method = $method;

        return $this;
    }
}