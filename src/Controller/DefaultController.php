<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\Type\TaskType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('home.html.twig', [
            'api_error_message' => !$this->isApiKeys() ? 'Twitter API keys not found in .env file' : null,
            'title' => 'Pega Test 1',
            'form' => $this->getForm()->createView()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->createForm(TaskType::class, new Task(), [
            'action' => 'submit',
            'method' => 'GET',
        ])->handleRequest($this->get('request_stack')->getCurrentRequest());
    }

    /**
     * @Route("/submit", name="homepage")
     *
     * @return Response|RedirectResponse
     */
    public function submit(): Response
    {
        $form = $this->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $url = $this->generateUrl('task', [
                    'handle1' => $task->getHandle1(),
                    'handle2' => $task->getHandle2(),
                    'method' => $task->getMethod()]
            );

            return $this->redirect($url);
        }

        return $this->index();
    }

    /**
     * @return bool
     */
    private function isApiKeys(): bool
    {
        return getenv('TWITTER_ACCESS_TOKEN') && getenv('TWITTER_ACCESS_TOKEN_SECRET') &&
            getenv('TWITTER_API_KEY') && getenv('TWITTER_API_SECRET');
    }
}