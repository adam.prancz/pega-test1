<?php

namespace App\Controller;

use App\Task\Entity\Post;
use App\Task\Enum\MethodEnum;
use ReflectionException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Service\TaskFactory;

class TaskController extends AbstractController
{
    /** @var TaskFactory|null
     */
    private $taskFactory = null;

    public function __construct(TaskFactory $taskFactory)
    {
        $this->taskFactory = $taskFactory;
    }

    /**
     * @Route("/{handle1}/{handle2}/{method}", name="task")
     * @param string      $handle1
     * @param string      $handle2
     * @param string|null $method
     *
     * @return Response
     * @throws ReflectionException
     */
    public function index(string $handle1, string $handle2 = null, string $method = null): Response
    {
        if ($handle1 == $handle2 || (!in_array($method, MethodEnum::getChoices()) && !is_null($method))) {
            return $this->redirect('/');
        }

        return $this->render('result.html.twig', [
            'title' => 'Pega Test 1 - Result',
            'handle1' => $handle1,
            'handle2' => $handle2,
            'method' => $method,
            'result' => $this->getResult($handle1, $handle2, $method),
            'empty_message' => 'No results has found...'
        ]);
    }

    /**
     * @param string      $handle1
     * @param string      $handle2
     * @param string|null $method
     *
     * @return array
     */
    public function getResult(string $handle1, string $handle2, string $method = null): array
    {
        $taskService = $this->taskFactory->get($handle1, $handle2, $method);

        $result = $taskService->doReplace();

        return array_map(function (Post $post, int $key) {
            return [
                'id' => $key,
                'source' => $post->getSource(),
                'time' => $post->getDisplayDate(),
                'message' => $post->getMessage()
            ];
        }, $result, array_keys($result));
    }
}