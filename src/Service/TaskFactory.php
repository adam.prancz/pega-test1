<?php

namespace App\Service;

use App\Task\Client\ChuckNorris;
use App\Task\Client\Twitter;
use App\Task\Enum\MethodEnum;
use App\Task\IndexGenerator\FibonacciGenerator;
use App\Task\IndexGenerator\ModGenerator;
use App\Task\TaskService;
use App\Task\Transformer\ChuckNorrisToPost;
use App\Task\Transformer\TwitterToPost;

class TaskFactory
{
    const NUMBER_OF_POSTS = 20;

    /** @var TaskService|null */
    private $taskService = null;

    /**
     * TaskFactory constructor.
     *
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param string      $handle1
     * @param string      $handle2
     * @param string|null $method
     *
     * @return TaskService
     */
    public function get(string $handle1, string $handle2, string $method = null): TaskService
    {
        $twitter1 = new Twitter();
        $twitter1
            ->setNumberOfPostsToGet(self::NUMBER_OF_POSTS)
            ->setResponseTransformer(new TwitterToPost())
            ->setHandle($handle1);

        $twitter2 = new Twitter();
        $twitter2
            ->setNumberOfPostsToGet(self::NUMBER_OF_POSTS)
            ->setResponseTransformer(new TwitterToPost())
            ->setHandle($handle2);

        $chuckNorris = new ChuckNorris();
        $chuckNorris
            ->setNumberOfPostsToGet((2 * self::NUMBER_OF_POSTS / 3))
            ->setResponseTransformer(new ChuckNorrisToPost());

        if ($method === MethodEnum::MOD) {
            $indexGenerator = new ModGenerator();
        } else {
            $indexGenerator = new FibonacciGenerator();
        }
        $indexGenerator->setNumberOfIndex(self::NUMBER_OF_POSTS * 2);

        $this->taskService->setSourceClient1($twitter1);
        $this->taskService->setSourceClient2($twitter2);
        $this->taskService->setSourceClient3($chuckNorris);
        $this->taskService->setIndexGenerator($indexGenerator);

        return $this->taskService;
    }
}