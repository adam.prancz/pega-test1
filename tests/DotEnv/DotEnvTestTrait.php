<?php

namespace App\Tests\DotEnv;

trait DotEnvTestTrait
{
    public final function testDotEnv(): void
    {
        if (!$this->isApiKeys()) {
            #$this->markTestSkipped('File .env is not available.');
            $this->fail('File .env is not available.');
        }

        $this->assertTrue(true);
    }

    /**
     * @return bool
     */
    private function isApiKeys(): bool
    {
        return getenv('TWITTER_ACCESS_TOKEN') && getenv('TWITTER_ACCESS_TOKEN_SECRET') &&
            getenv('TWITTER_API_KEY') && getenv('TWITTER_API_SECRET');
    }
}