<?php

namespace App\Tests\Behat;

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Exception;

class BaseTestContext extends MinkContext implements Context
{
    protected const ERROR_CONTENT_CAN_NOT_BE_FOUND = 'The content "%s" could not be found on the page';
    protected const ERROR_CONTENT_CAN_BE_FOUND = 'The content "%s" could be found on the page';

    /**
     * @Given I wait :secs second
     *
     * @param int $secs
     */
    public final function iWaitSecond(int $secs = 1): void
    {
        sleep($secs);
    }

    /**
     * @Given I should see (the) xpath :xpath
     *
     * @param string $xpath
     *
     * @return bool
     * @throws Exception
     */
    public function isXpathAvailable(string $xpath = ''): bool
    {
        $contents = $this->getSession()->getPage()->find('xpath', $xpath);

        if ($contents) {
            return true;
        }

        throw new Exception(sprintf(self::ERROR_CONTENT_CAN_NOT_BE_FOUND, $xpath));
    }

    /**
     * @Given I should not see (the) xpath :xpath
     *
     * @param string $xpath
     *
     * @return bool
     * @throws Exception
     */
    public function isXpathNotAvailable(string $xpath = ''): bool
    {
        $contents = $this->getSession()->getPage()->find('xpath', $xpath);

        if (!$contents) {
            return true;
        }

        throw new Exception(sprintf(self::ERROR_CONTENT_CAN_BE_FOUND, $xpath));
    }

    /**
     * @When I scroll :selector into view
     *
     * @param string $selector Allowed selectors: #id, .className, //xpath
     *
     * @throws Exception
     */
    public function scrollIntoView($selector): void
    {
        $locator = substr($selector, 0, 1);

        switch ($locator) {
            case '$' : // Query selector
                $selector = substr($selector, 1);
                $function = <<<JS
(function(){
  var elem = document.querySelector("$selector");
  elem.scrollIntoView(false);
})()
JS;
                break;

            case '/' : // XPath selector
                $function = <<<JS
(function(){
  var elem = document.evaluate("$selector", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
  elem.scrollIntoView(false);
})()
JS;
                break;

            case '#' : // ID selector
                $selector = substr($selector, 1);
                $function = <<<JS
(function(){
  var elem = document.getElementById("$selector");
  elem.scrollIntoView(false);
})()
JS;
                break;

            case '.' : // Class selector
                $selector = substr($selector, 1);
                $function = <<<JS
(function(){
  var elem = document.getElementsByClassName("$selector");
  elem[0].scrollIntoView(false);
})()
JS;
                break;

            default:
                throw new Exception(__METHOD__ . ' Couldn\'t find selector: ' . $selector . ' - Allowed selectors: #id, .className, //xpath');
                break;
        }

        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            throw new Exception(__METHOD__ . ' failed');
        }
    }
}
