<?php

namespace App\Test\Task;

use App\Task\Client\ChuckNorris;
use App\Task\Client\Twitter;
use App\Task\Entity\Post;
use App\Task\Enum\MethodEnum;
use App\Task\IndexGenerator\FibonacciGenerator;
use App\Task\TaskService;
use App\Task\IndexGenerator\ModGenerator;
use App\Task\Transformer\ChuckNorrisToPost;
use App\Task\Transformer\TwitterToPost;
use App\Tests\DotEnv\DotEnvTestTrait;
use PHPUnit\Framework\TestCase;

class TaskServiceTest extends TestCase
{
    use DotEnvTestTrait;

    const TEST_SOURCE = 'TEST';
    const START_TIME_STAMP = 1577836861;
    const TEST_MESSAGE = 'Test message'; // 2020-01-01 00:01:01

    /** @var TaskService|null */
    private $service = null;

    /** @var array */
    private $testPosts = [1, 2, 3, 4, 20];

    /** @var array */
    private $testHandles = [
        [
            'handle1' => 'symfony',
            'handle2' => 'knplabs'
        ]
    ];

    /**
     * TaskServiceTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->service = new TaskService();
    }

    public final function testCombArray(): void
    {
        $data1 = $this->generateTestArrays(1, self::START_TIME_STAMP, 1);
        $data2 = $this->generateTestArrays(1, self::START_TIME_STAMP + 10, 2);

        $result = $this->service->combArray($data1, $data2);

        $this->assertLessThan($result[1]->getTimeStamp(), $result[2]->getTimeStamp());

        $data1 = $this->generateTestArrays(1, self::START_TIME_STAMP + 10, 1);
        $data2 = $this->generateTestArrays(1, self::START_TIME_STAMP, 2);

        $result = $this->service->combArray($data1, $data2);

        $this->assertLessThan($result[1]->getTimeStamp(), $result[2]->getTimeStamp());
    }

    /**
     * @param int      $length
     * @param int|null $startTimeStamp
     * @param int      $startPostId
     * @param string   $source
     *
     * @return Post[]
     */
    public final function generateTestArrays(int $length = 4, int $startTimeStamp = null, int $startPostId = 1, string $source = ''): array
    {
        $testArray = [];

        for ($i = 0; $i < $length; $i++) {
            $post = new Post();
            $id = $startPostId ? $startPostId + $i : $i;
            $post
                ->setSource($source ?: self::TEST_SOURCE)
                ->setId($id)
                ->setMessage(self::TEST_MESSAGE . ' - ' . $id)
                ->setTimeStamp($startTimeStamp ? $startTimeStamp + $i : self::START_TIME_STAMP + $i);

            $testArray[] = $post;
        }

        return $testArray;
    }

    public final function testDoFibReplacements(): void
    {
        $this->testDotEnv();

        foreach ($this->testPosts as $value) {
            foreach ($this->testHandles as $handles) {
                $this->setService($handles['handle1'], $handles['handle2'], $value, MethodEnum::FIBONACCI);

                $result = $this->service->doReplace();

                $indexGenerator = new FibonacciGenerator();
                $indexGenerator->setNumberOfIndex(count($result));

                $this->checkReplacements($indexGenerator->getIndexes(), $result);
            }
        }
    }

    /**
     * @param string      $handle1
     * @param string      $handle2
     * @param int         $numberOfPosts
     * @param string|null $method
     */
    public final function setService(string $handle1, string $handle2, int $numberOfPosts = 1, string $method = null): void
    {
        $twitter1 = new Twitter();
        $twitter1
            ->setNumberOfPostsToGet($numberOfPosts)
            ->setResponseTransformer(new TwitterToPost())
            ->setHandle($handle1);

        $twitter2 = new Twitter();
        $twitter2
            ->setNumberOfPostsToGet($numberOfPosts)
            ->setResponseTransformer(new TwitterToPost())
            ->setHandle($handle2);

        $chuckNorris = new ChuckNorris();
        $chuckNorris
            ->setNumberOfPostsToGet((2 * $numberOfPosts / 3))
            ->setResponseTransformer(new ChuckNorrisToPost());

        if ($method === MethodEnum::MOD) {
            $indexGenerator = new ModGenerator();
        } else {
            $indexGenerator = new FibonacciGenerator();
        }
        $indexGenerator->setNumberOfIndex($numberOfPosts * 2);

        $this->service->setSourceClient1($twitter1);
        $this->service->setSourceClient2($twitter2);
        $this->service->setSourceClient3($chuckNorris);
        $this->service->setIndexGenerator($indexGenerator);
    }

    /**
     * @param int[]  $indexes
     * @param Post[] $result
     */
    public final function checkReplacements(array $indexes = [], array $result = []): void
    {
        $replacedNumber = 0;

        foreach ($indexes as $index) {
            if (isset($result[$index]) && $result[$index]->getSource() === 'icndb') {
                $this->assertTrue(true);
                $replacedNumber++;
            }
        }

        $this->assertEquals($replacedNumber, count($indexes));
    }

    public final function testDoModReplacement(): void
    {
        $this->testDotEnv();

        foreach ($this->testPosts as $value) {
            foreach ($this->testHandles as $handles) {
                $this->setService($handles['handle1'], $handles['handle2'], $value, MethodEnum::MOD);

                $result = $this->service->doReplace();

                $indexGenerator = new ModGenerator();
                $indexGenerator->setNumberOfIndex(count($result));

                $this->checkReplacements($indexGenerator->getIndexes(), $result);
            }
        }
    }
}