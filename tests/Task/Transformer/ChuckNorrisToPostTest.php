<?php

namespace App\Tests\Task\Transformer;

use App\Task\Client\ChuckNorris;
use App\Task\Transformer\ChuckNorrisToPost;
use Exception;

class ChuckNorrisToPostTest extends BaseTransformerTest
{
    /** @var ChuckNorris|null */
    protected $client = null;

    /**
     * ChuckNorrisToPostTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new ChuckNorris();
        $this->transformer = new ChuckNorrisToPost();
    }

    /**
     * @throws Exception
     */
    public final function transform(): void
    {
        parent::transform();

        $jokes = [];

        foreach (json_decode($this->data)->value as $joke) {
            $jokes[] = [
                'source' => 'icndb',
                'message' => $joke->joke
            ];
        }

        foreach ($jokes as $key => $joke) {
            if ($joke['source'] === $this->transformedData[$key]->getSource()) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Post source is not equal');
            }

            if ($joke['message'] === $this->transformedData[$key]->getMessage()) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Post message is not equal');
            }
        }
    }
}