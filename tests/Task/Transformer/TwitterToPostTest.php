<?php

namespace App\Tests\Task\Transformer;

use App\Task\Client\Twitter;
use App\Task\Transformer\TwitterToPost;
use App\Task\Entity\Post;
use DateTime;
use Exception;

class TwitterToPostTest extends BaseTransformerTest
{
    /** @var Twitter|null */
    protected $client = null;

    /** @var TwitterToPost|null */
    protected $transformer = null;

    /**
     * TwitterToPostTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new Twitter();
        $this->transformer = new TwitterToPost();
    }

    /**
     * @throws Exception
     */
    public final function transform(): void
    {
        parent::transform();

        $tweets = [];

        foreach (json_decode($this->data) as $tweet) {
            $date = new DateTime($tweet->created_at);
            $tweets[] = [
                'source' => 'twitter/' . $tweet->user->screen_name,
                'date' => $date->format(Post::DATE_FORMAT),
                'message' => $tweet->text
            ];
        }

        foreach ($tweets as $key => $tweet) {
            if ($tweet['source'] === $this->transformedData[$key]->getSource()) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Post source is not equal');
            }

            if ($tweet['date'] === $this->transformedData[$key]->getDisplayDate()) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Post date is not equal');
            }

            if ($tweet['message'] === $this->transformedData[$key]->getMessage()) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Post message is not equal');
            }
        }
    }
}