<?php

namespace App\Tests\Task\Transformer;

use App\Task\Client\ClientInterface;
use App\Task\Entity\Post;
use App\Task\Transformer\TransformerInterface;
use App\Tests\DotEnv\DotEnvTestTrait;
use PHPUnit\Framework\TestCase;
use Exception;

abstract class BaseTransformerTest extends TestCase
{
    use DotEnvTestTrait;

    /** @var TransformerInterface|null */
    protected $transformer = null;

    /** @var ClientInterface|null */
    protected $client = null;

    /** @var string */
    protected $data = '';

    /** @var Post[] */
    protected $transformedData = [];

    /** @var int */
    protected $numberOfTransforms = 5;

    /** @var array */
    protected $testPosts = [1, 2, 3, 4, 10, 20, 100];

    /**
     * @throws Exception
     */
    public final function testTransform(): void
    {
        foreach ($this->testPosts as $number) {
            $this->setNumberOfTransforms($number);
            $this->transform();
        }
    }

    public function transform(): void
    {
        $this->testDotEnv();

        $this->client->setNumberOfPostsToGet($this->getNumberOfTransforms());
        $this->data = $this->client->getAll();
        $this->transformedData = $this->transformer->transform($this->data);
    }

    /**
     * @return int
     */
    public final function getNumberOfTransforms(): int
    {
        return $this->numberOfTransforms;
    }

    /**
     * @param int $numberOfTransforms
     *
     * @return self
     */
    public final function setNumberOfTransforms(int $numberOfTransforms): self
    {
        $this->numberOfTransforms = $numberOfTransforms;

        return $this;
    }
}