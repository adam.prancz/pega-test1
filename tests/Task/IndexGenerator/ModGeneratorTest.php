<?php

namespace App\Tests\Task\IndexGenerator;

use App\Task\IndexGenerator\ModGenerator;

class ModGeneratorTest extends BaseIndexGeneratorTest
{
    /** @var ModGenerator|null */
    protected $generator = null;

    /**
     * IndexGeneratorTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->generator = new ModGenerator();
    }

    public final function testGetIndexes(): void
    {
        $modSeries = range(3, 100, 3);

        foreach ($this->testPosts as $value) {
            $this->generator->setNumberOfIndex($value);
            $result = $this->generator->getIndexes();

            if ($value <= 2) {
                $this->assertEquals([], $result);
            }

            if ($value > 2) {
                $this->assertEquals(array_slice($modSeries, 0, count($result)), $result);
            }
        }
    }
}