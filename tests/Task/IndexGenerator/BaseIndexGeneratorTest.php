<?php

namespace App\Tests\Task\IndexGenerator;

use App\Task\IndexGenerator\IndexGeneratorInterface;
use PHPUnit\Framework\TestCase;

abstract class BaseIndexGeneratorTest extends TestCase
{
    /** @var IndexGeneratorInterface|null */
    protected $generator = null;

    /** @var array */
    protected $testPosts = [1, 2, 3, 4, 10, 20, 100];

    public final function testSetAndGetNumberOfIndex(): void
    {
        $values = [1, 10, 20, 100];

        foreach ($values as $value) {
            $this->generator->setNumberOfIndex($value);
            $result = $this->generator->getNumberOfIndex();

            $this->assertEquals($value, $result);
        }
    }
}