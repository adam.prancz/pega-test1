<?php

namespace App\Tests\Task\IndexGenerator;

use App\Task\IndexGenerator\FibonacciGenerator;

class FibonacciGeneratorTest extends BaseIndexGeneratorTest
{
    /** @var FibonacciGenerator|null */
    protected $generator = null;

    /**
     * FibonacciGeneratorTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->generator = new FibonacciGenerator();
    }

    public final function testGetIndexes(): void
    {
        $fibonacciSeries = [3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418];

        foreach ($this->testPosts as $value) {
            $this->generator->setNumberOfIndex($value);
            $result = $this->generator->getIndexes();

            if ($value <=2) {
                $this->assertEquals([], $result);
            }

            if ($value > 2) {
                $this->assertEquals(array_slice($fibonacciSeries, 0, count($result)), $result);
            }
        }
    }
}