<?php

namespace App\Tests\Task\IndexGenerator;

use App\Task\Entity\Post;
use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    /** @var Post|null */
    protected $post = null;

    /**
     * FibonacciGeneratorTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->post = new Post();
    }

    /**
     * @throws Exception
     */
    public final function testGetDisplayDate(): void
    {
        $date = new DateTime();

        $this->post->setTimeStamp($date->getTimestamp());
        $result = $this->post->getDisplayDate();

        $this->assertEquals($date->format(Post::DATE_FORMAT), $result);
    }
}