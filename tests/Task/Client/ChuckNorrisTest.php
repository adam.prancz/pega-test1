<?php

namespace App\Tests\Task\Client;

use App\Task\Client\ChuckNorris;

class ChuckNorrisTest extends BaseClientTest
{
    /** @var ChuckNorris|null */
    protected $client = null;

    /**
     * ChuckNorrisTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new ChuckNorris();
    }

    public final function testGetAll(): void
    {
        $values = [1, 5, 20];

        foreach ($values as $value) {
            $this->client->setNumberOfPostsToGet($value);
            $result = count(json_decode($this->client->getAll())->value);

            $this->assertEquals($value, $result);
        }
    }
}