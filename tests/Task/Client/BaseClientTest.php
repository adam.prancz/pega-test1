<?php

namespace App\Tests\Task\Client;

use App\Task\Client\ClientInterface;
use App\Task\Transformer\ChuckNorrisToPost;
use App\Tests\DotEnv\DotEnvTestTrait;
use PHPUnit\Framework\TestCase;

abstract class BaseClientTest extends TestCase
{
    use DotEnvTestTrait;

    /** @var ClientInterface|null */
    protected $client = null;

    public final function testSetAndGetNumberOfPostsToGet(): void
    {
        $values = [1, 10, 20, 100];

        foreach ($values as $value) {
            $this->client->setNumberOfPostsToGet($value);
            $result = $this->client->getNumberOfPostsToGet();

            $this->assertEquals($value, $result);
        }
    }

    public final function testSetAndGetResponseTransformer(): void
    {
        $testTransformer = new ChuckNorrisToPost();

        $this->client->setResponseTransformer($testTransformer);
        $result = $this->client->getResponseTransformer();

        $this->assertEquals($testTransformer, $result);
    }

    public function testGetAll(): void
    {
        $this->testDotEnv();

        $values = [1, 5, 20];

        foreach ($values as $value) {
            $this->client->setNumberOfPostsToGet($value);
            $result = count(json_decode($this->client->getAll()));

            $this->assertEquals($value, $result);
        }
    }
}