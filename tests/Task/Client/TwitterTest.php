<?php

namespace App\Tests\Task\Client;

use App\Task\Client\Twitter;

class TwitterTest extends BaseClientTest
{
    /** @var Twitter|null */
    protected $client = null;

    /**
     * ChuckNorrisTest constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new Twitter();
    }
}